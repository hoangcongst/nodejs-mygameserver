var http = require('http');
var express = require('express');
var app = express();
var serve = http.createServer(app);
var io = require('socket.io')(serve);
var mongoose = require('mongoose');
serve.listen(3000, function() {
    console.log('Express server listening on port 3000');
});
//connect to mongodb
mongoose.connect('mongodb://127.0.0.1:27017/demo');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
var roomSchema = mongoose.Schema({
    name: String,
    listUser: Array
});
var userSchema = mongoose.Schema({
    user_id: String,
    name: String
});
var Room = mongoose.model('Room', roomSchema);
var User = mongoose.model('User', userSchema);
//connected to mongodb
io.on('connection', function(socket) {
    console.log('a user connected');
    // db.once('open', function() {
    Room.find(function(err, listRoom) {
        if (err) return console.error(err);
        socket.emit('connection', listRoom);
    });
    // });
    socket.on('disconnect', function() {
        console.log('user disconnected');
    });
    socket.on('createRoom', function(msg) {
        var name = msg.name;
        var user_id = msg.user_id;
        User.findOne({
            user_id: user_id
        }, function(err, user) {
            if (err) return handleError(err);
            var newRoom = new Room({
                name: name,
                listUser: [user]
            });
            newRoom.save(function(err, fluffy) {
                if (err) return console.error(err);
            });
            io.emit('newRoom', newRoom);
            socket.emit('createRoom', newRoom);
        });
    });
    socket.on('outRoom', function(msg) {
        var user_id = msg.user_id,
            room_id = msg.room_id;
        console.log(user_id + " log out room " + room_id);
        Room.findById(room_id, function(err, room) {
            if (err) return handleError(err);
            if (room.listUser.length == 1) {
                room.remove(function(err, room) {
                    if (err) return handleError(err);
                });
                var updateOutRoom = {
                    room_id: room_id,
                    user_id: user_id
                };
                io.emit('updateOutRoom', updateOutRoom);
            } else {
                User.findOne({
                    user_id: user_id
                }, function(err, user) {
                    if (err) return handleError(err);
                    Room.findByIdAndUpdate(room_id, {
                        $pull: {
                            listUser: user
                        }
                    }, {
                        safe: true,
                        upsert: true,
                        new: true
                    }, function(err, room) {
                        if (err) {
                            return handleError(err)
                        } else {
                            var outRoom = {
                                action: 'outRoom',
                                user_id: user_id
                            };
                            console.log('emit to ownerRoom');
                            io.emit(room_id, outRoom);
                            var updateOutRoom = {
                                room_id: room_id,
                                user_id: user_id
                            };
                            io.emit('updateOutRoom', updateOutRoom);
                        }
                    });
                });
            }
        });
    });
    socket.on('joinRoom', function(msg) {
        var user_id = msg.user_id;
        console.log(user_id);
        var room_id = msg.room;
        User.findOne({
            user_id: user_id
        }, function(err, user) {
            if (err) return handleError(err);
            console.log(user);
            Room.findByIdAndUpdate(room_id, {
                $push: {
                    listUser: user
                }
            }, {
                safe: true,
                upsert: true,
                new: true
            }, function(err, room) {
                if (err) {
                    socket.emit("resultJoinRoom", "error");
                    return handleError(err)
                } else {
                    //send to all other client
                    var resultJoinRoom = {
                        room_id: room_id,
                        // ownerRoom: room.listUser[0];
                        user: user
                    };
                    //send to only user in room
                    var joinRoom = {
                        action: 'joinRoom',
                        room: room
                    }
                    io.emit("resultJoinRoom", resultJoinRoom);
                    io.emit(room_id, joinRoom);
                }
            });
        });
    });
    //http handle add user when he/she online
    app.get('/login/:user_id/:user_name', function(req, res) {
        var user_id = req.params.user_id,
            user_name = req.params.user_name;
        var ownerRoom = new User({
            user_id: user_id,
            name: user_name
        });
        ownerRoom.save(function(err, fluffy) {
            if (err) console.log(err);
        });
        res.send('hello world');
    });
});