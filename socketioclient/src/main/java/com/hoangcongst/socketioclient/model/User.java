/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hoangcongst.socketioclient.model;

/**
 *
 * @author hoangcongst
 */
public class User {

    private String name;
    private String userId;

    public String getName() {
        return name;
    }

    public User(String name, String userId) {
        this.name = name;
        this.userId = userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
