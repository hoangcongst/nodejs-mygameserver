package com.hoangcongst.socketioclient;

import com.hoangcongst.socketioclient.model.Room;
import com.hoangcongst.socketioclient.model.User;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hoangcongst
 */
public class Main extends javax.swing.JFrame {

    public static final Logger LOG = Logger.getLogger(Main.class.getName());
    private DefaultListModel<Room> model = new DefaultListModel<Room>();
    RoomUI mRoomUi = new RoomUI(Main.this);

    public ListModel<Room> getModel() {
        return model;
    }

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        this.setTitle(StaticVar.userName);
        LOG.log(Level.INFO, StaticVar.userName);
        try {
            // TODO add your handling code here:
            mSocket = IO.socket("http://localhost:3000");
            mSocket.connect();

            // Receiving all current rooms when connect to server
            mSocket.on("connection", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONArray rooms = (JSONArray) args[0];
                    int length = rooms.length();

                    // create JList with model
                    for (int i = 0; i < length; i++) {
                        try {
                            JSONObject jsonObj = (JSONObject) rooms.get(i);
                            Room room = new Room(jsonObj.getString("_id"), jsonObj.getString("name"));

                            JSONArray listUserJson = (JSONArray) jsonObj.get("listUser");

                            for (int j = 0; j < listUserJson.length(); j++) {
                                JSONObject userJson = (JSONObject) listUserJson.get(j);
                                room.getListUser().add(new User(userJson.getString("name"), userJson.getString("user_id")));
                            }
                            model.addElement(room);
                        } catch (JSONException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                    listRoom.setModel(model);
                }
            });

            //update user into room to all users
            mSocket.on("resultJoinRoom", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject resultJoinRoom = (JSONObject) args[0];
                    for (int i = 0; i < model.getSize(); i++) {
                        Room room = model.get(i);
                        try {
                            if (room.getId().equals(resultJoinRoom.getString("room_id"))) {
                                JSONObject userJson = (JSONObject) resultJoinRoom.get("user");
                                room.getListUser().add(new User(userJson.getString("name"), userJson.getString("user_id")));
                                listRoom.updateUI();
                                break;
                            }
                        } catch (JSONException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });

            //callback when you create room
            mSocket.on("createRoom", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject roomJson = (JSONObject) args[0];
                    LOG.log(Level.INFO, roomJson.toString());
                    try {
                        Room room = new Room(roomJson.getString("_id"), roomJson.getString("name"));
                        JSONArray listUserJson = (JSONArray) roomJson.get("listUser");

                        for (int j = 0; j < listUserJson.length(); j++) {
                            JSONObject userJson = (JSONObject) listUserJson.get(j);
                            room.getListUser().add(new User(userJson.getString("name"), userJson.getString("user_id")));
                        }
                        //Show RoomUi
                        Main.this.setVisible(false);
                        mRoomUi = new RoomUI(Main.this);
                        mRoomUi.getUser1().setText(room.getListUser().get(0).getName());
                        mRoomUi.setVisible(true);
                        updateCrrRoom(room);
                    } catch (JSONException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            //when a room is created
            mSocket.on("newRoom", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject roomJson = (JSONObject) args[0];
                    try {
                        Room room = new Room(roomJson.getString("_id"), roomJson.getString("name"));
                        JSONArray listUserJson = (JSONArray) roomJson.get("listUser");

                        for (int j = 0; j < listUserJson.length(); j++) {
                            JSONObject userJson = (JSONObject) listUserJson.get(j);
                            room.getListUser().add(new User(userJson.getString("name"), userJson.getString("user_id")));
                        }

                        model.addElement(room);
                    } catch (JSONException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            //handle all other client when someone out room
            mSocket.on("updateOutRoom", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject updateOutRoom = (JSONObject) args[0];
                    try {
                        String user_id = updateOutRoom.getString("user_id");
                        String room_id = updateOutRoom.getString("room_id");
                        int length = model.getSize();
                        for (int i = 0; i < length; i++) {
                            Room room = model.get(i);
                            int roomUserNum = room.getListUser().size();
                            if (room.getId().equals(room_id)) {
                                if (roomUserNum == 1) {
                                    model.removeElementAt(i);
                                } else {
                                    for (int j = 0; j < roomUserNum; j++) {
                                        if (room.getListUser().get(j).getUserId().equals(user_id)) {
                                            room.getListUser().remove(j);
                                            break;
                                        }
                                    }
                                    listRoom.updateUI();
                                }
                                break;
                            }
                        }
                    } catch (JSONException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

        } catch (URISyntaxException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        listRoom.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list = (JList) evt.getSource();
                if (evt.getClickCount() == 2) {
                    try {
                        // Double-click detected
                        int index = list.locationToIndex(evt.getPoint());
                        // Sending an object
                        Room room = model.get(index);
                        int numUser = room.getListUser().size();
                        if (numUser < 2) {
                            JSONObject obj = new JSONObject();
                            obj.put("room", room.getId());
                            obj.put("user_id", StaticVar.userId);
                            mSocket.emit("joinRoom", obj);
                            Main.this.setVisible(false);
                            mRoomUi = new RoomUI(Main.this);
                            mRoomUi.setVisible(true);
                            mRoomUi.getUser1().setText(room.getListUser().get(0).getName());
                            mRoomUi.getUser2().setText(StaticVar.userName);
                            updateCrrRoom(room);
                        } else {
                            JOptionPane.showMessageDialog(null, "Room Full!", "Alert: ", JOptionPane.INFORMATION_MESSAGE);
                        }
                    } catch (JSONException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCreateRoom = new javax.swing.JButton();
        txtRoom = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        listRoom = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnCreateRoom.setText("Create Room");
        btnCreateRoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateRoomActionPerformed(evt);
            }
        });

        listRoom.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listRoom.setToolTipText("");
        listRoom.setCellRenderer(new CustomList());
        jScrollPane1.setViewportView(listRoom);
        listRoom.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtRoom, javax.swing.GroupLayout.DEFAULT_SIZE, 537, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCreateRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreateRoom)
                    .addComponent(txtRoom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 408, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static Socket mSocket;

    private void updateCrrRoom(Room room) {
        try {
            mSocket.off(StaticVar.crrRoom.getId());
        } catch (Exception e) {
            LOG.log(Level.INFO, "have never join any room");
        }
        StaticVar.crrRoom = room;
        //listen to separate room
        mSocket.on(StaticVar.crrRoom.getId(), new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject joinRoom = (JSONObject) args[0];
                try {
                    String action = joinRoom.getString("action");
                    if (action.equals("joinRoom")) {
                        JSONObject roomJson = (JSONObject) joinRoom.get("room");
                        Room room = new Room(roomJson.getString("_id"), roomJson.getString("name"));
                        JSONArray listUserJson = (JSONArray) roomJson.get("listUser");

                        for (int j = 0; j < listUserJson.length(); j++) {
                            JSONObject userJson = (JSONObject) listUserJson.get(j);
                            room.getListUser().add(new User(userJson.getString("name"), userJson.getString("user_id")));
//                            LOG.log(Level.INFO, "update user " + j);
                            if (j == 0) {
                                mRoomUi.getUser1().setText(room.getListUser().get(0).getName());
                            } else {
                                mRoomUi.getUser2().setText(room.getListUser().get(1).getName());
                            }
                        }
                        StaticVar.crrRoom = room;
                    } else if (action.equals("outRoom")) {
                        String user_out = joinRoom.getString("user_id");
                        for (int i = 0; i < StaticVar.crrRoom.getListUser().size(); i++) {
                            if (user_out.equals(StaticVar.crrRoom.getListUser().get(i).getUserId())) {
                                StaticVar.crrRoom.getListUser().remove(i);
                                if (i == 0) {
                                    mRoomUi.getUser1().setText("");
                                } else {
                                    mRoomUi.getUser2().setText("");
                                }
                                break;
                            }
                        }
                    }
                } catch (JSONException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    private void btnCreateRoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateRoomActionPerformed
        try {
            // Sending an object
            JSONObject obj = new JSONObject();
            obj.put("user_id", StaticVar.userId);
            obj.put("name", txtRoom.getText());
            mSocket.emit("createRoom", obj);

        } catch (JSONException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_btnCreateRoomActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateRoom;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<Room> listRoom;
    private javax.swing.JTextField txtRoom;
    // End of variables declaration//GEN-END:variables
}
