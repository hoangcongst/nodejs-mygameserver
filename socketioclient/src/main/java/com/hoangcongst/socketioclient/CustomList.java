/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hoangcongst.socketioclient;

import com.hoangcongst.socketioclient.model.Room;
import static java.awt.AWTEventMulticaster.add;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.print.Book;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class CustomList extends JPanel implements ListCellRenderer<Room> {

//    private JLabel lbIcon = new JLabel();
    private JLabel lbName = new JLabel();
    private JLabel lbAuthor = new JLabel();

    public CustomList() {
        setLayout(new BorderLayout(5, 5));

        JPanel panelText = new JPanel(new GridLayout(0, 1));
        panelText.add(lbName);
        panelText.add(lbAuthor);
//        add(lbIcon, BorderLayout.WEST);
        add(panelText, BorderLayout.CENTER);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Room> list,
            Room room, int index, boolean isSelected, boolean cellHasFocus) {

//        lbIcon.setIcon(new ImageIcon(getClass().getResource(
//                "/com/mycompany/socketioclient/iconroom.jpeg")));
        lbName.setText(room.getName());
        lbName.setFont(new Font("Serif", Font.BOLD, 16));
        int numUser = room.getListUser().size();
        lbAuthor.setText("Số người chơi: " + numUser + "/2");
        lbAuthor.setForeground(Color.blue);
        lbAuthor.setFont(new Font("Serif", Font.BOLD, 14));

        // set Opaque to change background color of JLabel
        lbName.setOpaque(true);
        lbAuthor.setOpaque(true);
//        lbIcon.setOpaque(true);

        // when select item
        if (isSelected) {
            lbName.setBackground(Color.YELLOW);
            lbAuthor.setBackground(Color.YELLOW);
//            lbIcon.setBackground(list.getSelectionBackground());
            setBackground(Color.YELLOW);
        } else { // when don't select
            lbName.setBackground(Color.WHITE);
            lbAuthor.setBackground(Color.WHITE);
//            lbIcon.setBackground(list.getBackground());
            setBackground(Color.WHITE);
        }
        return this;
    }
}
